package com.example.projetapplication.apiclass;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Index;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ArtsDAO {
    @Query("SELECT * FROM favTable")
    LiveData<List<Datum>> getFavoriteArts();

    @Insert (onConflict = OnConflictStrategy.IGNORE)
    void insert(Datum data);

    @Delete
    void remove(Datum data);

}
