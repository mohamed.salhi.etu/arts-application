package com.example.projetapplication.apiclass;


import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Root{
    public Pagination getPagination() {
        return this.pagination; }
    public void setPagination(Pagination pagination) {
        this.pagination = pagination; }
    Pagination pagination;
    public ArrayList<Datum> getData() {
        return this.data; }
    public void setData(ArrayList<Datum> data) {
        this.data = data; }
    ArrayList<Datum> data;
    public Info getInfo() {
        return this.info; }
    public void setInfo(Info info) {
        this.info = info; }
    Info info;
    public Config getConfig() {
        return this.config; }
    public void setConfig(Config config) {
        this.config = config; }
    Config config;
}
