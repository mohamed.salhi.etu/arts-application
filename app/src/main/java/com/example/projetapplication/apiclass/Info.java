package com.example.projetapplication.apiclass;

import java.util.ArrayList;

public class Info{
    public String getLicense_text() {
        return this.license_text; }
    public void setLicense_text(String license_text) {
        this.license_text = license_text; }
    String license_text;
    public ArrayList<String> getLicense_links() {
        return this.license_links; }
    public void setLicense_links(ArrayList<String> license_links) {
        this.license_links = license_links; }
    ArrayList<String> license_links;
    public String getVersion() {
        return this.version; }
    public void setVersion(String version) {
        this.version = version; }
    String version;
}
