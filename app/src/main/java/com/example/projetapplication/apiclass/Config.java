package com.example.projetapplication.apiclass;

public class Config{
    public String getIiif_url() {
        return this.iiif_url; }
    public void setIiif_url(String iiif_url) {
        this.iiif_url = iiif_url; }
    String iiif_url;
    public String getWebsite_url() {
        return this.website_url; }
    public void setWebsite_url(String website_url) {
        this.website_url = website_url; }
    String website_url;
}
