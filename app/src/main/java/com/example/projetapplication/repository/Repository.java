package com.example.projetapplication.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.projetapplication.API_Client;
import com.example.projetapplication.API_Interface;
import com.example.projetapplication.apiclass.ArtsDAO;
import com.example.projetapplication.apiclass.Datum;
import com.example.projetapplication.apiclass.Root;
import com.example.projetapplication.room.ArtsDatabase;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    private MutableLiveData<List<Datum>> allArts;
    private static Repository instance;

    public ArtsDAO artsDAO;
    private LiveData<List<Datum>> favoriteArts;


    public Repository(Context context){
        allArts= new MutableLiveData<>();
        ArtsDatabase database = ArtsDatabase.getArtsDatabase(context);
        artsDAO = database.artsDao();
        favoriteArts = artsDAO.getFavoriteArts();

    }

    public void getAllArts(){
        API_Interface apiService = API_Client.getClient().create(API_Interface.class);
        Call<Root> call = apiService.getData();


        call.enqueue(new Callback<Root>() {
                         @Override
                         public void onResponse(Call<Root> call, Response<Root> response) {
                             Log.d("MS", "on response");
                             allArts.postValue(response.body().getData());
                         }

                         @Override
                         public void onFailure(Call<Root> call, Throwable t) {
                             Log.d("MS", "on failure");
                         }
                     }
        );
    }

    public MutableLiveData<List<Datum>> getArts(){
        return allArts;

    }

    public void insert(Datum data) {
        artsDAO.insert(data);
    }

    public void remove(Datum data){
        artsDAO.remove(data);
    }

    public LiveData<List<Datum>> getFavoriteArts(){
        return this.favoriteArts;
    }
}
