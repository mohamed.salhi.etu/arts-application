package com.example.projetapplication;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetapplication.apiclass.Datum;

import java.util.List;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.projetapplication.viewmodels.MainViewModel;


public class RecyclerAdapter  extends RecyclerView.Adapter<RecyclerAdapter.MyviewHolder> {
    private List<Datum> liste_data;
    private Context context;
    private MainViewModel viewModel;
    private int layoutId;

    public RecyclerAdapter(Context applicationContext, List<Datum> liste_data, MainViewModel viewModel,int layoutId) {
        this.liste_data = liste_data;
        this.context = applicationContext;
        this.viewModel = viewModel;
        this.layoutId = layoutId;
        ;

    }

    public void setDataList(List<Datum> liste_arts) {
        this.liste_data = liste_arts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerAdapter.MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.MyviewHolder holder, int position) {
        holder.title.setText(liste_data.get(position).getTitle());
        holder.auteur.setText(liste_data.get(position).getArtistDisplay());
        String url = "https://www.artic.edu/iiif/2/"+liste_data.get(position).getImageId()+"/full/843,/0/default.jpg";
        Log.d("MS", url);
        Glide.with(context)
                .load(url)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.image);


        //Start new details activity when we click on the image
        ImageView image = holder.image;
        (image).setOnClickListener(a-> {
            Intent intent = new Intent(a.getContext(),DetailsActivity.class);

            intent.putExtra("author",liste_data.get(position).getArtistTitle());
            intent.putExtra("title",liste_data.get(position).getTitle());
            intent.putExtra("history",liste_data.get(position).getMediumDisplay());
            intent.putExtra("image_id",liste_data.get(position).getImageId());

            (a.getContext()).startActivity(intent);

        });

        ImageButton favButton = holder.favButton;
        (favButton).setOnClickListener(a-> {
            viewModel.insertArt(liste_data.get(position));

        });


    }

    @Override
    public int getItemCount() {
        if (liste_data != null) {
            return liste_data.size();
        }
        return 0;
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView auteur;
        private ImageView image;
        ImageButton favButton;
        public MyviewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            auteur = (TextView) itemView.findViewById(R.id.auteur);
            image = (ImageView) itemView.findViewById(R.id.image);
            favButton = (ImageButton) itemView.findViewById(R.id.buttonF);

        }


    }
}
