package com.example.projetapplication;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.util.Log;

import com.example.projetapplication.fragments.ArtsView;
import com.example.projetapplication.fragments.FavoriteView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager2 viewPager2 = (ViewPager2) findViewById(R.id.viewPager2);
        viewPager2.setAdapter(new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                Fragment frag = null;
                switch(position) {
                    case 0:
                        frag = new ArtsView();
                        break;

                    case 1:
                        frag = new FavoriteView();
                        break;

                    default:
                        Log.d("MS", "Erreur au moment de changer de fragment");

                }
                return frag;
            }

            @Override
            public int getItemCount() {
                return 2;
            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //new TabLayoutMediator(tabLayout, viewPager2, (tab, position) -> tab.setText("OBJECT " + (position + 1))).attach();
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                if (position == 0){
                    tab.setText("Liste des oeuvres" );
                }
                else{
                    tab.setText("Favoris");
                }
            }
        });
        tabLayoutMediator.attach();

    }
}