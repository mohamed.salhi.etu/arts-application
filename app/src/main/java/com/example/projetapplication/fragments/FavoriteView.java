package com.example.projetapplication.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projetapplication.FavRecyclerAdapter;
import com.example.projetapplication.R;
import com.example.projetapplication.RecyclerAdapter;
import com.example.projetapplication.apiclass.Datum;
import com.example.projetapplication.viewmodels.MainViewModel;

import java.util.List;


public class FavoriteView extends Fragment {

    private List<Datum> liste_data;
    private RecyclerView recyclerView;
    private FavRecyclerAdapter recyclerAdapter;
    private LinearLayoutManager linearLayoutManager;
    private MainViewModel viewModel;
    private View view;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fav_item_layout,container,false);

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.init(this.getActivity().getApplication());

        viewModel.getAllArts();

        viewModel.getFavCards().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {
            @Override
            public void onChanged(List<Datum> data) {

                if (data!=null){
                    liste_data = data;
                    recyclerAdapter = new FavRecyclerAdapter(getContext(),data,viewModel);
                    recyclerView = view.findViewById(R.id.myRecyclerView);

                    linearLayoutManager= new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);

                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(recyclerAdapter);



                }
            }
        });


        return view;
}
}