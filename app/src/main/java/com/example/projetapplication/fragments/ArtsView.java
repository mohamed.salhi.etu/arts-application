package com.example.projetapplication.fragments;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.projetapplication.API_Client;
import com.example.projetapplication.API_Interface;
import com.example.projetapplication.R;
import com.example.projetapplication.RecyclerAdapter;
import com.example.projetapplication.apiclass.Datum;
import com.example.projetapplication.apiclass.Root;
import com.example.projetapplication.viewmodels.MainViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArtsView extends Fragment {

    private List<Datum> liste_data;

    private RecyclerView recyclerView;
    private RecyclerView recyclerViewGrid;

    private RecyclerAdapter recyclerAdapter;
    private RecyclerAdapter recyclerGridAdapter;

    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;

    private MainViewModel viewModel;
    private View view;
    private View view2;
    private Context context;
    private ImageView button;
    private boolean is_grid =false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.item_layout,container,false);

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.init(this.getActivity().getApplication());
        //viewModel = new ViewModelProvider(this,ViewModelProvider.AndroidViewModelFactory.getInstance().get(MainViewModel.class));

        viewModel.getAllArts();


        viewModel.getArtsList().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {
            @Override
            public void onChanged(List<Datum> data) {
                if (data!=null){
                    liste_data = data;

                    recyclerAdapter = new RecyclerAdapter(getContext(),data,viewModel,R.layout.fragment_cards_view);
                    recyclerGridAdapter = new RecyclerAdapter(getContext(),data,viewModel,R.layout.fragments_arts_view_grid);

                    recyclerView = view.findViewById(R.id.myRecyclerView);



                    linearLayoutManager= new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);
                    gridLayoutManager = new GridLayoutManager(view.getContext(),2);

                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(recyclerAdapter);

                    button = view.findViewById(R.id.buttonGrid);


                    (button).setOnClickListener(a-> {
                        if(is_grid == false) {
                            is_grid=true;
                            button.setImageResource(R.drawable.ic_baseline_view_list_24);
                            recyclerView.setAdapter(recyclerGridAdapter);
                            recyclerView.setLayoutManager(gridLayoutManager);
                        }
                        else{
                            is_grid=false;
                            button.setImageResource(R.drawable.ic_baseline_view_module_24);
                            recyclerView.setAdapter(recyclerAdapter);
                            recyclerView.setLayoutManager(linearLayoutManager);

                        }



                    });








                }
            }
        });


        return view;


    }



}

