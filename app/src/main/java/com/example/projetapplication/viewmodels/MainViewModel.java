package com.example.projetapplication.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.projetapplication.apiclass.Datum;
import com.example.projetapplication.repository.Repository;

import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private Repository repository;
    private MutableLiveData<List<Datum>> artsList;
    private LiveData<List<Datum>> favoriteArts ;

    public MainViewModel(@NonNull Application application){
        super(application);


    }
    public void init(Application application){
        repository = new Repository(application);
        repository.getAllArts();
        artsList=repository.getArts();
        favoriteArts = repository.getFavoriteArts();

    }

    public void getAllArts(){
        repository.getAllArts();
    }

    public LiveData<List<Datum>> getArtsList() {
        return artsList;
    };

    public LiveData<List<Datum>> getFavCards(){return favoriteArts;}

    public void insertArt(Datum data){repository.insert(data);}

    public void removeArt(Datum data){repository.remove(data);}

}
