package com.example.projetapplication;



import com.example.projetapplication.apiclass.Datum;
import com.example.projetapplication.apiclass.Root;


import java.util.List;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;

import io.reactivex.SingleConverter;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface API_Interface {
    @GET("api/v1/artworks")
    //Call<Single<GithuUser>> getData(@Query("limit") int limit_size);
    Call<Root> getData();
}

