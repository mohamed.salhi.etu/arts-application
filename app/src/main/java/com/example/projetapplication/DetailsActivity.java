package com.example.projetapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.projetapplication.fragments.ArtsView;

public class DetailsActivity extends AppCompatActivity {
    String author;
    String title;
    String history;
    String image_id;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Intent intent = getIntent();
        if (intent != null){
            Log.d("MS","intent not null");
            if (intent.hasExtra("author")){
                author = intent.getStringExtra("author");
                TextView textAuthor = findViewById(R.id.Author);
                textAuthor.setText(author);

            }
            if (intent.hasExtra("title")){
                title = intent.getStringExtra("title");
                TextView textTitle = findViewById(R.id.Title);
                textTitle.setText(title);

            }
            if (intent.hasExtra("history")){
                history = intent.getStringExtra("history");
                TextView textHistory = findViewById(R.id.History);
                textHistory.setText(history);

            }
            if (intent.hasExtra("image_id")){
                image_id = intent.getStringExtra("image_id");
                String url = "https://www.artic.edu/iiif/2/"+image_id+"/full/843,/0/default.jpg";
                ImageView image = findViewById(R.id.imageView);
                Glide.with(getApplicationContext())
                        .load(url)
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(image);
            }
        }



    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d("MS","DetailsActicity détruit");

    }

    @Override
    public void onPause(){
        super.onPause();
        Log.d("MS","DetailsActicity en pause");
    }
}