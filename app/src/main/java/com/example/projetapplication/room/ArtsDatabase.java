package com.example.projetapplication.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.projetapplication.apiclass.ArtsDAO;
import com.example.projetapplication.apiclass.Datum;

@Database(entities ={Datum.class},version = 2,exportSchema = true )
public abstract class ArtsDatabase extends RoomDatabase {
    public static ArtsDatabase instance;
    public ArtsDAO artsDAO;



    public static ArtsDatabase getArtsDatabase(Context context){
        if(instance==null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), ArtsDatabase.class, "favTable")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
        }

    public abstract ArtsDAO artsDao();


    }

